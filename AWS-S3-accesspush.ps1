

#Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Force
Import-Module -Name AWSPowerShell #.NetCore 
Add-Type -AssemblyName AWSSDK.S3
# Your account access key - must have read access to your S3 Bucket
#$accessKey = 'AKIAVSIWXIGW6E6DT2G5'
# Your account secret access key
#$secretKey = 'j27riW+D98NI4aEpI7MJfDkDZexLgZfFssCsNdzV'

#$config=New-Object Amazon.S3.AmazonS3Config
# The region associated with your bucket
$Region = 'us-east-1'
#$application="Eng Ops Sbox SPB"
# The name of your S3 Bucket
$bucket = 'ags3outputtest'



# The folder in your bucket to copy, including trailing slash. Leave blank to copy the entire bucket
$TargetPath1 = 'InTrust-AirGap-share/S3UploadTest/'


# The local file path where files should be copied
$localPath = "$env:HOMEDRIVE\Build_Outputs\InTrust\\11.4.1\\AIX_11.4.1.4499_test"
$localPath = $localPath -replace '\\\\', '\'
$localPath = $localPath -replace '\\', '\\'
$Localitems = @(Get-ChildItem -Path $localPath -Recurse) 

#$objects = Get-S3Object -BucketName $bucket -KeyPrefix $TargetPath1 -AccessKey $accessKey -SecretKey $secretKey -Region $Region

Measure-Command -Expression {
  foreach($localFileName in $Localitems) 
  {
    #Write-Host -Object $localFileName.FullName
     
     
    [string]$filename = $localFileName.fullname -replace $localPath, ''
    $filename = $filename -replace '\\', '/'
    #Write-Host -Object "filename: $filename"
    $object = "$TargetPath1$filename"
    $object = $object -replace '//', '/'
    #Write-Host -Object "Object: $object"
    
    if ($object -ne '') 
    {
      $localFilePath = $localFileName.fullname
      $localFilePath = $localFilePath -replace '\\\\', '\'
      Write-Host -Object $localFilePath
      try 
      {
        Write-S3Object -BucketName $bucket -Key $object -File $localFilePath  #-Region $Region  #-AccessKey $accessKey -SecretKey $secretKey
        #    
      }

      catch  [InvalidOperationException]
      {
        # get error record
        [Management.Automation.ErrorRecord]$e = $_

        # retrieve information about runtime error
        $info = [PSCustomObject]@{
          Exception = $e.Exception.Message
          Reason    = $e.CategoryInfo.Reason
          Target    = $e.CategoryInfo.TargetName
          Script    = $e.InvocationInfo.ScriptName
          Line      = $e.InvocationInfo.ScriptLineNumber
          Column    = $e.InvocationInfo.OffsetInLine
          ErrorCode = $e.Exception.ErrorCode
        }
        if (!($info.Reason = 'InvalidOperationException')) 
        {
          #output information. Post-process collected info, and log info (optional)
          Write-Error -Message $info 
        }
      }
    }
  }
}