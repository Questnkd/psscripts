#requires -Version 2.0 -Modules AWSPowerShell
# PowerShell function Script: set AWSprofile
# Author: Kelly Davis
# version: 1.0

function script:AWSS3profile 
{ Import-Module -Name AWSPowerShell
  param (
    [parameter(Mandatory, helpmessage = 'Your AWS -AccessKey is required')] [string]$AccessKey,
    [parameter(Mandatory, helpmessage = 'Your AWS -SecretKey is required')] [string]$SecretKey,
    [parameter(Mandatory, helpmessage = 'you must provide a name to store the credentals')][string]$StoreAs
    
  )
  Import-Module -Name AWSPowerShell
  Set-AWSCredential -AccessKey $AccessKey -SecretKey $SecretKey -StoreAs $StoreAs
} 

