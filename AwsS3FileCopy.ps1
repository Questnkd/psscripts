# requires -Version 3.0 -Modules AWSPowerShell
# PowerShell function example script: moveing code in and out of AWS buckets
# Author: Kelly Davis
# version: 1.0
# Version: 1.1 udpate correcting sinlge file copy from root and files with spaces in there names
# version: 1.2 added file and path list from local location and AWS location
# Version: 2 Added support to use AWS Profiles rather then access keys. - Keys now packaged in EXE 
# version: 2 Bug: corrected parcing error in copy up - files not being located after root location
# Version: 2 Added automatic error correction in user input - added support for user input error
# version: 2 Bug: corrected issue in bucket and Keypretix handleing - added empty bucket
# version: 2 Bug: corrected issue in Error Catch - all errors useing same out value   



function script:AwsS3FileCopy 
{
  [CmdletBinding()]
  param (
    #[parameter(Mandatory = $true, helpmessage = 'Your AWS -AccessKey is required')] [string]$AccessKey,
    #[parameter(Mandatory = $true, helpmessage = 'Your AWS -SecretKey is required')] [string]$SecretKey,
    [parameter(Mandatory, helpmessage = 'your AWS -Profile is required')][string]$AWSprofile,
    [parameter(Mandatory, helpmessage = 'The target buckets AWS -Region is required')] [string]$Region,
    [string]$bucketin,
    [string]$bucketout,
    [parameter(Mandatory, helpmessage = 'The AWS Rootkey or "folder name" for -TargetPath is required' )] [string]$Targets,
    [parameter(Mandatory, helpmessage = 'The local target directory for -LocalPath is required')] [string]$LocalPath
  )
    
  begin 
  {  
    $localPath = $localPath -replace '\\\\', '\'
    $localPath = $localPath -replace '\\', '\\'

    #Write-Output -InputObject "AccessKey = $AccessKey"
    #Write-Output -InputObject "SecretKey = $SecretKey"
    Write-Output -InputObject "Region = $Region"
    Write-Output -InputObject "bucketin = $bucketin"
    Write-Output -InputObject "bucketout = $bucketout"
    Write-Output -InputObject "TargetPath = $Targets"
    Write-Output -InputObject "LocalPath = $LocalPath"
    Write-Output -InputObject "Profile = $AWSprofile"
    Write-Output -InputObject ' '
    
    
    
    [bool]$in = $false
    [bool]$out = $false

    If ($bucketin -notlike '')
    {
      [bool]$in = $true
      $bucket = $bucketin
    }
    if ($bucketout -notlike '')
    {
      [bool]$out = $true
      $bucket = $bucketout
    }
    IF($in -eq $false -and $out -eq $false)
    {
      Write-Host  -Object 'Please define your AWS bucket and rerun script'
      exit
    }
  }

   
    
  process 
  { 
    #Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Force
    Import-Module -Name AWSPowerShell
    Add-Type -AssemblyName AWSSDK.S3
    Write-Host  -Object ' '
    Write-Host  -Object 'Check AWS S3 access'
    Write-Host  -Object ' '
    Initialize-AWSDefaultConfiguration -ProfileName $AWSprofile -Region $Region
    Get-AWSCredential -ListProfileDetail
    Write-Host  -Object ' '
    foreach($TargetPath in $Targets)
    {
      try
      {
        Get-S3Object -BucketName $bucket -KeyPrefix $TargetPath 
      }
      catch
      {
        # get error record
        [Management.Automation.ErrorRecord]$a = $_
		
        # retrieve information about runtime error
        $info = [PSCustomObject]@{
          Exception = $a.Exception.Message
          Reason    = $a.CategoryInfo.Reason
          Target    = $a.CategoryInfo.TargetName
          Script    = $a.InvocationInfo.ScriptName
          Line      = $a.InvocationInfo.ScriptLineNumber
          Column    = $a.InvocationInfo.OffsetInLine
          ErrorCode = $a.Exception.ErrorCode
        }
        # output information. Post-process collected info, and log info (optional)
        Write-Error  $info
        Exit 
      }
    }  
    if ($out -eq $true)
    {
      foreach($TargetPath in $Targets)
      {
        Write-Host  -Object "Pulling files from $bucketout in Region $Region"
        $objects = Get-S3Object -BucketName $bucketout -KeyPrefix $TargetPath 
        Measure-Command -Expression 
        {
          foreach($object in $objects) 
          { 
            $localFileName = $object.Key -replace $keyPrefix, ''
    
            if ($localFileName -ne '') 
            {
              $localFilePath = Join-Path -Path $LocalPath -ChildPath $localFileName
              try
              {
                Copy-S3Object -BucketName $bucket -Key $object.Key -LocalFile $localFilePath -Region $Region #-AccessKey $AccessKey -SecretKey $SecretKey 
              }
    
              catch  [Amazon.S3.AmazonS3Exception]
              {
                # get error record
                [Management.Automation.ErrorRecord]$b = $_

                # retrieve information about runtime error
                $info = [PSCustomObject]@{
                  Exception = $b.Exception.Message
                  Reason    = $b.CategoryInfo.Reason
                  Target    = $b.CategoryInfo.TargetName
                  Script    = $b.InvocationInfo.ScriptName
                  Line      = $b.InvocationInfo.ScriptLineNumber
                  Column    = $b.InvocationInfo.OffsetInLine
                  ErrorCode = $b.Exception.ErrorCode
                }
                if (!($info.Reason = 'InvalidOperationException')) 
                {
                  # output information. Post-process collected info, and log info (optional)
                  Write-Error  $info 
                }
              }
            }
          }
        }
      }
    }
    if ($in -eq $true)
    {
      foreach($TargetPath in $Targets)
      {
        Write-Host  -Object "copying files to $bucketin in Region $Region"
        $Localitems = @(Get-ChildItem -Path $LocalPath -Recurse) 
        Measure-Command -Expression {
          foreach($localFileName in $Localitems) 
          { 
            #Write-Host -Object $localFileName.FullName            
            [string]$filename = $localFileName.fullname -replace $LocalPath,''
            $filename = $filename -replace '\\', '/'
            #Write-Host -Object "filename: $filename"
            $object = "$TargetPath$filename"
            $object = $object -replace '//', '/'
            #Write-Host -Object "Object: $object"
    
            if ($object -ne '') 
            {
              $localFilePath = $localFileName.fullname
              $localFilePath = $localFilePath -replace '\\\\', '\'
              # Write-Host -Object $localFilePath
              try 
              { #write-host "the copy line -BucketName $bucket -Key $object -File $localFilePath"
                Write-S3Object -BucketName $bucket -Key $object -File $localFilePath 
                #    
              }

              catch  [InvalidOperationException]
              {
                # get error record
                [Management.Automation.ErrorRecord]$c = $_

                # retrieve information about runtime error
                $info = [PSCustomObject]@{
                  Exception = $c.Exception.Message
                  Reason    = $c.CategoryInfo.Reason
                  Target    = $c.CategoryInfo.TargetName
                  Script    = $c.InvocationInfo.ScriptName
                  Line      = $c.InvocationInfo.ScriptLineNumber
                  Column    = $c.InvocationInfo.OffsetInLine
                  ErrorCode = $c.Exception.ErrorCode
                }
                if (!($info.ErrorCode = 'InvalidOperationException')) 
                {
                  #output information. Post-process collected info, and log info (optional)
                  Write-Error  $info 
                }
              }
            }
          }
        }
      }
    }
  }
  end 
  { 
    $listme = Get-ChildItem -Path $LocalPath -Recurse
    #write-host "list me says: " $listme
    #Write-host ""
    foreach($_ in $listme)
    {
      Write-Host -Object ''
      Write-Output -InputObject $_.fullname
    }
  
    $listme2 = Get-S3Object -BucketName $bucket -KeyPrefix $Targets 
    #Write-Host  "List me 2 says: $listme2"
    #Write-Host  ''
    foreach($_ in $listme2)
    {
      Write-Host -Object ''
      Write-Output -InputObject $_.Key
    }    
  }
}
